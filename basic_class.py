class Employee:

    # initialize class attribute.
    def __init__(self, first, last, pay):
        self.first = first
        self.last = last
        self.pay = pay
        self.email = first + '.' + last + '@itwox.com'

    # Print full name using method
    def fullname(self):
        return '{} {}'.format(self.first, self.last)

    def name_pay(self):
        return '{} = {}'.format(self.first, self.pay)


emp_1 = Employee('vinay', 'singh', 5000)
emp_2 = Employee("aditiya", "roy", "$100K")
emp_3 = Employee('vivek', 'agnihotri', 5400)

# print(emp_1.first)
# print(emp_2.email)
# print(emp_1.fullname())

# Print only method or function
print(emp_1.fullname())
print(emp_2.name_pay())
# Print class with method
print(Employee.fullname(emp_2))

# print(emp_1.first + emp_1.email)


# emp_1.first = 'rajendra'
# emp_1.last = 'Reddy'
# emp_1.email = "rakreddy@gmail.com"
# emp_1.pay = 50000
